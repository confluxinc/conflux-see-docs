========================
REST API Ejemplos
========================

Ejemplos en diferentes lenguajes de programación.

C# 
----------

Usando la libreria RestSharp http://restsharp.org/

.. code-block:: Java

		var client = new RestClient("http://<su-ip-publica>");
		// client.Authenticator = new HttpBasicAuthenticator(username, password);

		var request = new RestRequest("conflux-see/api/documents/invoice/", Method.GET);

		// Autenticación via URL Apikey
		request.AddHeader("X-CONFLUX-API-KEY", "<tu-clave-privada>");

		// Ejecute la solicitud
		IRestResponse response = client.Execute(request);
		var content = response.Content; // contenido como cadena

		// Automaticamente deserializar
		// El tipo de contenido devuelto es deserializado pero se puede establecer explícitamente a través de RestClient.AddHandler ();
		RestResponse<Person> response2 = client.Execute<Person>(request);
		var name = response2.Data.Name;

		// Modo asincrono
		client.ExecuteAsync(request, response => {
		    Console.WriteLine(response.Content);
		});

		// Modo asincrono con deserializacion
		var asyncHandle = client.ExecuteAsync<Person>(request, response => {
		    Console.WriteLine(response.Data.Name);
		});

		// abort the request on demand
		asyncHandle.Abort();

Delphi 
----------

Usando la libreria Delphi Rest Client Api https://github.com/chinshou/delphi-rest-client-api
ejemplo::
	//Prmero declaramos un objeto para una correcta deserializacion del retorno
	TPerson = class(TObject)
	public 
		(* Reflect the java object field names, case-sensitive *)
		id: Integer;
		name: String;
		email: String;

		(* utility function *)
		class function NewFrom(Id: Integer; Name, EMail: String): TPerson;
	end;
	//Enlistamos los resultados obtenidos por un servicio rest
	var
		vLista : TList<TPerson>;
	begin
		vLista := RestClient.Resource('http://<su-ip-publica>/conflux-see/api/documents/invoice/')
							.Accept(RestUtils.MediaType_Json)
							.Header('X-CONFLUX-API-KEY', '<tu-clave-privada>')//Clave privada otorgada una vez instalado el sistema
							.GetAsList<TPerson>();