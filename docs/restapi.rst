========================
REST API Documentation
========================

El sistema de facturación electrónica estará disponible mediante un servicio rest para conexion via autenticación via clave privada (private key) ``http://<su-ip-publica>/conflux-see/`` endpoint. Esta API requiere autenticación.

Servicios 
----------

El recurso Servicios representa todos los servicios web actualmente rastreados.

===================   ===============
Parametro             Descripción
===================   ===============
X-CONFLUX-API-KEY     Clave privada otorgada por el vendedor (SHA-1)
===================   ===============

Listado de Facturas y Boletas
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: text

    http://<su-ip-publica>/index.php/api/documents/invoice/

GET
+++++

Retorna una lista de todas las facturas y boletas

.. code-block:: bash

   $ curl -X GET -H "X-CONFLUX-API-KEY: <tu_clave_api>" http://<su-ip-publica>/index.php/api/documents/invoice/format/json

.. code-block:: js

        {
            "status": "success",
            "message":"",
            "rpta": [
                {
                    "_id": "5967b379c81bc336d692ae18",
                    "tipo_doc": "RUC",
                    "cliente_doc": "20266352337",
                    "cliente_domic": "AV. SANTA ROSA NRO. 550 URB.  SANTA ANITA SANTA ANITA - LIMA - LIMA",
                    "cliente_nomb": "PRODUCTOS TISSUE DEL PERU S.A. O PROTISA-PERU",
                    "codigo_barras": "10292501515 | 01 | F001 | 29 | 601.13 | 3940.73 | 2017-07-13 | 6 | 20266352337
                    | oKDdibK+QTSwXWFVQRf/tW2SBdc= | Sebeo8KZobdEkam6SD4TRFUL0OakTzV7hnZK8H37tzUCHFYY1/ZCPn+pMU/8AdMbXArrWHMOeai8LdcxO3tIjM8lDCO6dlPqGZLocPnTY8EkFc
                    +hM9U0DTZZMWy+Xx36t7LI2OtK/DZilAJ7OIG2sMjtmHu8DZ0Nc9wCiifB3slSp3Z44tSNNaQy1vmlLoyV7K4Q2zGb7q3xDuJEYJJYYN2ytrP9l79YhHzqGRe
                    /29/IUgytn5LoLdxCWrJHaL070Yokvxlju0XV03Gt1fzJUoy8MMWtzs8hVrCOYWyu5p3ldoDH1ahdc0VMswKlL5aOhM+5ySGE3ypfh8Z
                    /7dNmzg==",
                    "descuento_global": 0.0,
                    "estado": "ES",
                    "estado_baja": "",
                    "estado_resumen": "",
                    "fecemi": "2017-07-13",
                    "fecven": "2017-07-13",
                    "metadata": [],
                    "moneda": "PEN",
                    "motivo_baja": "",
                    "numero": 29,
                    "observ": "DOC.TRANSP.N\u00ba 16596903",
                    "porcentaje_detraccion": "4",
                    "porcentaje_detraccion_valor": 0,
                    "porcentaje_igv": 18.0,
                    "ruta_cdr_xml": "see-files/cdr/R-10292501515-01-F001-29.xml",
                    "ruta_pdf": "see-files/pdf/10292501515-01-F001-29.pdf",
                    "ruta_xml_firmado": "see-files/firma/10292501515-01-F001-29.xml",
                    "ruta_zip_firmado": "see-files/firma/10292501515-01-F001-29.zip",
                    "serie": "F001",
                    "sunat_description": "La Factura numero F001-29, ha sido aceptada",
                    "sunat_faultcode": "",
                    "sunat_note": "",
                    "sunat_responsecode": "0",
                    "sunat_soap_error": "",
                    "supplier": {
                        "conflux_departamento": "AREQUIPA", 
                        "conflux_direccion": "AV. UNION NRO. 200 PAMPA CAMARONES AREQUIPA - AREQUIPA - SACHACA", 
                        "conflux_distrito": "YANAHUARA", 
                        "conflux_provincia": "AREQUIPA", 
                        "conflux_razon_social": "VALENCIA GONZALES ROBERTO HIPOLITO", 
                        "conflux_ruc": "10292501515", 
                        "conflux_ubigeo": "040117", 
                        "nota_firma": "Elaborado por Sistema de Emision Electronica Facturador CONFLUX (SEE-CONFLUX) 1.0.0", 
                        "resolucion_intendencia": "0520050000277"
                    }, 
                    "tipo": "F", 
                    "tipo_cambio": 0, 
                    "tipo_oper": "01", 
                    "total": 3940.73, 
                    "total_desc": 0.0, 
                    "total_detraccion": 157.62912, 
                    "total_igv": 601.13, 
                    "total_isc": 0.0, 
                    "total_ope_exoneradas": 0, 
                    "total_ope_gratuitas": 0, 
                    "total_ope_gravadas": 3339.6, 
                    "total_ope_inafectas": 0, 
                    "total_otros_cargos": 0.0, 
                    "total_otros_tributos": 0.0
                },
                {
                    ...
                }
            ]
        }

POST 
++++++

Crea una nueva Factura/Boleta y retorna un objeto.

==============   ===============
Param            Description
==============   ===============
data             Cadena de texto en formato JSON codificada en Base 64 
==============   ===============

.. code-block:: text

   $ curl -X POST -H "X-CONFLUX-API-KEY: <tu_clave_api>" -D "data=<base-64-json-encoded>" http://<su-ip-publica>/index.php/api/documents/invoice/

Información codificada en Base 64

.. code-block:: js


        {
            "tipo":"F",
            "porcentaje_igv":"18",
            "tipo_oper":"01",
            "fecemi":"2017-07-14",
            "fecven":"2017-07-14",
            "serie":"FF11",
            "moneda":"PEN",
            "descuento_global":"0",
            "observ":"",
            "items":[
                {
                    "codigo":"",
                    "descr":"asdasdsad",
                    "cod_unidad":"NIU",
                    "unidad":"number of international units",
                    "cant":"1",
                    "gratuito":"false",
                    "valor_unitario":"0.85",
                    "valor_gratuito":"0",
                    "precio_venta_unitario":"1.00",
                    "igv":"0.15",
                    "isc":"0",
                    "descuento":"0",
                    "otros_cargos":"0",
                    "otros_tributos":"0",
                    "tipo_igv":"10",
                    "importe_total":"1"
                },
                {
                    ...
                }
            ],
            "tipo_doc":"RUC",
            "cliente_doc":"20601207592",
            "cliente_nomb":"V Y B TRANSERVI SAC",
            "cliente_domic":"CAL.JOSE GALVEZ NRO. 322 MIRAFLORES - AREQUIPA - AREQUIPA"
        }
        
El tipo de datos que se envian a través de la solicitud antes mencionada serán del siguiente tipo

=======================   ===============
Parámetro                 Tipo de Dato
=======================   ===============
fecemi                    varchar(10) Fecha de emision del comprobante en formato YYYY-MM-DD
fecven                    varchar(10) Fecha de vencimiento del comprobante en formato YYYY-MM-DD
tipo                      varchar(2) [F=>Factura, B=>Boleta de Venta, ND=>Nota de Débito, NC=>Nota de Crédito]
serie                     varchar(4)
moneda                    varchar(3) [PEN=>"SOLES PERUANOS", USD=>"DOLARES AMERICANOS", EUR=>"EUROS"]
cliente_nomb              varchar(100) {en caso de ser anónimo (Boleta de Venta) deberá ir "-"}
cliente_domic             varchar(100) {en caso de ser anónimo (Boleta de Venta) deberá ir "-"}
cliente_doc               varchar(15) {en caso de ser anónimo (Boleta de Venta) deberá ir "-"}
tipo_doc                  varchar(3) [-=>Clientes varios o anonimos, 0=>Sin Documento, DNI=>Documento Nacional de Identidad, RUC=>Registro Único del Contribuyente, CED=>Cédula Diplomática de Identidad, PAS=>Pasaporte, CAE=>Carnet de Extranjería]
items                     array {n items}
'items.codigo'            varchar(30)
'items.descr'             varchar(250)
'items.cod_unidad'        varchar(4) {se utiliza el catálogo entregado por la SUNAT en el siguiente <a href="http://orientacion.sunat.gob.pe/index.php/empresas-menu/comprobantes-de-pago-empresas/comprobantes-de-pago-electronicos-empresas/see-desde-los-sistemas-del-contribuyente/2-comprobantes-que-se-pueden-emitir-desde-see-sistemas-del-contribuyente/boleta-de-venta-electronica-desde-sistemas-del-contribuyente/3641-detalle-de-catalogos-del-anexo-8-catalogo-de-codigos">enlace</a>.}
'items.unidad'            varchar(100)
'items.cant'              varchar(13)
'items.inafecto'          boolean
'items.exonerada'         boolean
'items.gratuito'          boolean
'items.valor_unitario'    varchar(13)
'items.valor_gratuito'    varchar(13)
'items.precio_venta_unitario'varchar(13)
'items.igv'               varchar(13)
'items.isc'               varchar(13)
'items.otros_tributos'    varchar(13)
'items.desc'              varchar(13) {se refiere al total de descuentos en el item}

=======================   ===============

Respuesta positiva del Servicio

.. code-block:: js

        {
            "status": "success",
            "message": "Mensaje personalizadp",
            "data": {
                
            }
        }
        {
            "data": {
                "_id": "59690336a5810851ae1794cf", 
                "cliente_doc": "20454171749", 
                "cliente_domic": "AV. ARANCOTA NRO. 100 SACHACA - AREQUIPA - AREQUIPA", 
                "cliente_nomb": "PEPE LUCHO\u00b4S S.R.L.", 
                "codigo_barras": "", 
                "codigo_barras_pdf": "", 
                "descuento_global": 0.0, 
                "digest_value": "", 
                "estado": "BO", 
                "estado_baja": "", 
                "estado_resumen": "", 
                "fecemi": "Fri, 14 Jul 2017 00:00:00 GMT", 
                "fecven": "Fri, 14 Jul 2017 00:00:00 GMT", 
                "items": [
                    {
                    "cant": 1.0, 
                    "cod_unidad": "NIU", 
                    "codigo": "", 
                    "descr": "sdfdsf", 
                    "descuento": 0.0, 
                    "gratuito": false, 
                    "igv": 0.15, 
                    "isc": 0.0, 
                    "otros_cargos": 0.0, 
                    "otros_tributos": 0.0, 
                    "precio_venta_unitario": 1.0, 
                    "tipo_igv": "10", 
                    "unidad": "number of international units", 
                    "valor_gratuito": 0, 
                    "valor_unitario": 0.85, 
                    "valor_venta": 0.85
                    }
                ], 
                "metadata": [], 
                "moneda": "PEN", 
                "motivo_baja": "", 
                "observ": "", 
                "porcentaje_detraccion": "", 
                "porcentaje_detraccion_valor": 0, 
                "porcentaje_igv": 18.0, 
                "ruta_cdr_xml": "", 
                "ruta_pdf": "", 
                "ruta_xml_firmado": "", 
                "ruta_zip_firmado": "", 
                "serie": "FF11", 
                "signature_value": "", 
                "sunat_description": "", 
                "sunat_faultcode": "", 
                "sunat_note": "", 
                "sunat_responsecode": "", 
                "sunat_soap_error": "", 
                "supplier": {
                "conflux_departamento": "MOQUEGUA", 
                "conflux_direccion": "CAL.ILO NRO. 1005 (EX GRIFO CHEN CHEN) MOQUEGUA - MARISCAL NIETO - MOQUEGUA", 
                "conflux_distrito": "MOQUEGUA", 
                "conflux_provincia": "MARISCAL NIETO", 
                "conflux_razon_social": "EMPRESA DE SERVICIOS M\u00daLTIPLES BRALEX E.I.R.L.", 
                "conflux_ruc": "20449443919", 
                "conflux_ubigeo": "180101", 
                "nota_firma": "Elaborado por Sistema de Emision Electronica Facturador CONFLUX (SEE-CONFLUX) 1.0.0", 
                "resolucion_intendencia": "XXXXX"
                }, 
                "tipo": "F", 
                "tipo_cambio": 0, 
                "tipo_doc": "RUC", 
                "tipo_oper": "01", 
                "total": 1.0, 
                "total_desc": 0.0, 
                "total_detraccion": 0, 
                "total_igv": 0.15, 
                "total_isc": 0.0, 
                "total_ope_exoneradas": 0, 
                "total_ope_gratuitas": 0, 
                "total_ope_gravadas": 0.85, 
                "total_ope_inafectas": 0, 
                "total_otros_cargos": 0.0, 
                "total_otros_tributos": 0.0
            }, 
            "message": "El comprobante fue guardado correctamente sin un numero correlativo", 
            "status": "success"
        }

Respuesta negativa del Servicio

.. code-block:: js

        {
            "status": "error",
            "message": "<Mensaje personalizado de acuerdo al error ocurrido>",
            "data": {
                "estado":"<BO|FI|ES>"
                "sunat_description": "<Mensaje otorgado por SUNAT>", 
                "sunat_faultcode": "<Codigo de error fault en consulta SOAP>", 
                "sunat_note": "<Codigos de observaciones por SUNAT>", 
                "sunat_responsecode": "<Codigo de respuesta de proceso por SUNAT>", 
                "sunat_soap_error": "<Codigo de error en conexion SOAP>", 
            },
        }


COMUNICACIÓN DE BAJA 
--------------------

Si existen FACTURAS O BOLETAS DE VENTA electrónicas que no han sido entregadas a  sus clientes, pueden ser dadas de baja a través de una comunicación a la SUNAT.


.. code-block:: text

    http://<su-ip-publica>/index.php/api/void/invoice/format/json/

POST
+++++

.. code-block:: bash

   $ curl -X GET -H "X-CONFLUX-API-KEY: <tu_clave_api>" -D "data=<base-64-json-encoded>" http://<su-ip-publica>/index.php/api/void/invoice/format/json/

Información codificada en Base 64

.. code-block:: js

        {
            "_id": "_id del comprobante",
            "fec": "2017-01-01",
            "motivo_baja":"MOTIVO DE LA BAJA"
        }


RESUMEN DIARIO
--------------

Es la información correspondiente a las boletas de venta y notas de crédito y debito electrónicas emitidas en un determinado día. Los archivos electrónicos no son enviados a la SUNAT, estos son conservados por el emisor.


.. code-block:: text

    http://<su-ip-publica>/index.php/api/summary/invoice/format/json/

POST
+++++

.. code-block:: bash

   $ curl -X GET -H "X-CONFLUX-API-KEY: <tu_clave_api>" -D "data=<base-64-json-encoded>" http://<su-ip-publica>/index.php/api/summary/invoice/format/json/

Información codificada en Base 64

.. code-block:: js

        {
            "fecres": "2017-01-01"
        }

