.. Conflux SEE documentation master file, created by
   sphinx-quickstart on Thu Nov 21 00:03:13 2011.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

==========
Conflux SEE
==========

Acerca de
==========

El sistema de emisión de comprobantes electrónicos es un software basado en web con soporte en varios sistemas operativos que soporten HTTP Server, MySQL, OpenSSL.

User's Guide
=============

Una completa guia del uso e integración con sistemas externos.

.. toctree::
   :maxdepth: 2

   quickstart
   help
   restapi
   examples



